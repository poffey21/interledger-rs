# Interledger RS

### Tim's Aim!

* [x] Leverage git submodule to point to an [open-source rust-based app](https://github.com/interledger-rs/interledger-rs)
* [x] [pull down the repo](https://gitlab.com/poffey21/interledger-rs/-/blob/master/.gitlab-ci.yml#L3-4)
* [x] [build it](https://gitlab.com/poffey21/interledger-rs/-/blob/master/.gitlab-ci.yml#L22)
* [ ] [run](https://gitlab.com/poffey21/interledger-rs/-/blob/master/.gitlab-ci.yml#L23) our [rust fuzzer](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/#supported-fuzzing-engines-and-languages) on it
* [ ] Push results into GitLab
